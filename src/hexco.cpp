#include "hexco.h"
#include "ui_hexco.h"

Hexco::Hexco(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Hexco)
{
    ui->setupUi(this);
	int j = 1;
	for(int i = 31; i > -1; i--)
	{
		QVBoxLayout* layout = new QVBoxLayout();
		ui->horizontalLayout->addLayout(layout);
		
		QString n = QString::number(i);
		QLabel* label = new QLabel(n);
		layout->addWidget(label);
		QCheckBox* bit = new QCheckBox;
		connect(bit, SIGNAL(toggled(bool)), SLOT(updateHex()));
		layout->addWidget(bit);
		bits.append(bit);
		
		if(j == 4 && i != 0)
		{
			ui->horizontalLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
			j = 0;
		}
		j++;
	}
	connect(ui->caps, SIGNAL(toggled(bool)), SLOT(updateHex()));
	connect(ui->clearBtn, SIGNAL(clicked(bool)), SLOT(clearBits()));
	connect(ui->setAllBtn, SIGNAL(clicked(bool)), SLOT(setAll()));
	this->updateHex();
}

Hexco::~Hexco()
{
    delete ui;
}

void Hexco::updateHex()
{
	QString sbinary = "";
	QList<QCheckBox*>::iterator it;
	for (it = this->bits.begin(); it != this->bits.end(); it++)
	{
		if((*it)->checkState())
			sbinary += "1";
		else
			sbinary += "0";
	}
	bool ok;
	QString shex = (QString::number(sbinary.toUInt(&ok, 2),16));
	if(ui->caps->checkState())
		shex = shex.toUpper();
	shex = "0x" + shex;
	if(ok)
		ui->outputLE->setText(shex);
	else
		ui->outputLE->setText("Error");
}

void Hexco::clearBits()
{
	QList<QCheckBox*>::iterator it;
	for (it = this->bits.begin(); it != this->bits.end(); it++)
	{
		(*it)->setChecked(false);
	}
}

void Hexco::setAll()
{
	QList<QCheckBox*>::iterator it;
	for (it = this->bits.begin(); it != this->bits.end(); it++)
	{
		(*it)->setChecked(true);
	}
}
