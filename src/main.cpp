#include "hexco.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Hexco w;
    w.setWindowIcon(QIcon(":/icons/icons/256x256/hexco.png"));
    w.show();

    return app.exec();
}
