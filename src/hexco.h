#ifndef HEXCO_H
#define HEXCO_H

#include <QMainWindow>
#include <QtGui>
#include <QLabel>
#include <QLayout>
#include <QCheckBox>
#include <QSpacerItem>

namespace Ui {
	class Hexco;
}

class Hexco : public QMainWindow
{
	Q_OBJECT

public:
	explicit Hexco(QWidget *parent = 0);
	~Hexco();

private:
	Ui::Hexco *ui;
	QList<QCheckBox*> bits;
private slots:
	void updateHex();
	void clearBits();
	void setAll();
};

#endif // HEXCO_H
